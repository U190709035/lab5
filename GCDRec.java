import java.util.Scanner;

public class GCDRec {
    public static void main(String[] args)
    {
        System.out.println(gcd(Integer.parseInt(args[0]),Integer.parseInt(args[1])));

    }

    static int gcd(int num1, int num2) {
        if (num2 == 0)
            return num2;
        if (num2 == 0)
            return num1;

        if (num1 == num2)
            return num1;

        if (num1 > num2)
            return gcd(num1-num2, num2);
        return gcd(num1, num2-num1);
    }


}